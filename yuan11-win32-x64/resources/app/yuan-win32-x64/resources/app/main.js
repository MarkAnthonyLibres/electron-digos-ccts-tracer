console.log("main process working");

const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const path = require("path");
const url = require("url");
var curl = require('curlrequest');
const PDFWindow = require('electron-pdf-window')
const ipc = electron.ipcMain;
const dialog = electron.dialog;

let win;

function createWindow()
{
    win = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true
        }
    });

    win.loadURL(url.format({
        pathname: path.join(__dirname, 'view/index.html'),
        protocol : 'file',
        slashes : true
    }));

    win.on('closed', () => {
        win = null;
    })

}


app.on("ready", createWindow);

ipc.on("trace_email", function (event, data) {

    var d = new Date();
    d.setDate(d.getDate() - (data.days));

    let query = {
        "Email" : data.email,
        "start" : {"$gte" : d.toISOString()}
    };

    query = JSON.stringify(query);

    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webSecurity: false,
        title : "EXPORT PDF",
        show : false
    });


    win.loadURL('http://127.0.0.1:8000/api/pdf', {
        postData: [{
            type: 'rawData',
            bytes: Buffer.from('query=' + query)
        }],
        extraHeaders: 'Content-Type: application/x-www-form-urlencoded'
    });

    win.webContents.on('did-finish-load', function() {
        win.show();
        win.maximize()
        event.sender.send("trace_email_done");
    });
});

app.on("window-all-closed", function () {
    if(process.platform !== "darwin")
    {
        app.quit();
    }

})

app.on('activate', function () {
    if (win==null)
        createWindow();
})